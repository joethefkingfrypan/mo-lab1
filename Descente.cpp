#include <windows.h>
#include "Entete.h"
#include <string>

#pragma comment (lib,"DescenteDLL.lib")  

// ========================================================================================== //
//                                           README                                           //
// ========================================================================================== //

// Le fichier de problème (.txt) doit se trouver dans le répertoire courant du projet pour une exécution à l'aide du compilateur.
// Les fichiers de la DLL (GeneticDLL.dll et GeneticDLL.lib) doivent se trouver dans le même répertoire que l'exécutable (.exe-DEBUG) et 
// dans le répertoire courant du projet pour une exécution à l'aide du compilateur.
// Indiquer les arguments du programme dans les propriétés du projet - débogage - arguements.
// Sinon, utiliser le répertoire execution.

// ========================================================================================== //
//                                     EXTERNAL PROTOYPES                                     //
// ========================================================================================== //

//DESCRIPTION:	Lecture du Fichier probleme et initialiation de la structure Problem
extern "C" _declspec(dllimport) void LectureProbleme(std::string FileName, TProblem & unProb, TAlgo &unAlgo);

//DESCRIPTION:	Fonction d'affichage à l'écran permettant de voir si les données du fichier problème ont été lues correctement
extern "C" _declspec(dllimport) void AfficherProbleme (TProblem unProb);

//DESCRIPTION: Affichage d'une solution a l'écran pour validation
extern "C" _declspec(dllimport) void AfficherSolution(const TSolution uneSolution, const TAlgo unAlgo, int N, std::string Titre);

//DESCRIPTION: Affichage à l'écran de la solution finale, du nombre d'évaluations effectuées et de certains paramètres
extern "C" _declspec(dllimport) void AfficherResultats (const TSolution uneSol, TProblem unProb, TAlgo unAlgo);

//DESCRIPTION: Affichage dans un fichier(en append) de la solution finale, du nombre d'évaluations effectuées et de certains paramètres
extern "C" _declspec(dllimport) void AfficherResultatsFichier (const TSolution uneSol, TProblem unProb, TAlgo unAlgo, std::string FileName);

//DESCRIPTION:	Évaluation de la fonction objectif d'une solution et MAJ du compteur d'évaluation. Retourne un long représentant le cout total de transit (flux*distances)
extern "C" _declspec(dllimport) long EvaluerSolution(const TSolution uneSolution, TProblem unProb, TAlgo &unAlgo);

//DESCRIPTION:	Création d'une séquence aléatoire (permutation) et évaluation de la fonction objectif. Allocation dynamique de mémoire pour la séquence (.Seq)
extern "C" _declspec(dllimport) void CreerSolutionAleatoire(TSolution & uneSolution, TProblem unProb, TAlgo &unAlgo);

//DESCRIPTION: Copie de la séquence et de la fonction objectif dans une nouvelle TSolution. La nouvelle TSolution est retournée.
extern "C" _declspec(dllimport) void CopierSolution (const TSolution uneSol, TSolution &Copie, TProblem unProb);

//DESCRIPTION:	Libération de la mémoire allouée dynamiquement
extern "C" _declspec(dllimport) void	LibererMemoireFinPgm	(TSolution uneCourante, TSolution uneNext, TSolution uneBest, TProblem unProb);

// ========================================================================================== //
//                                         PROTOTYPES                                         //
// ========================================================================================== //

//DESCRIPTION: Méthode générique permettant l'exécution de différentes méthodes de résolution
void findBestAlternative(TSolution &current, TSolution &next, TSolution &best, TProblem instance, TAlgo &algo, Type type);
void invokeAlgorithm(TSolution &current, TSolution &next, TSolution &best, TProblem instance, TAlgo &algo, Type type);

//DESCRIPTION: Création d'une solution voisine à partir de la solution uneSol. NB:uneSol ne doit pas être modifiée
TSolution GetSolutionVoisine (const TSolution current, TProblem instance, TAlgo &algo);
TSolution GetSolutionVoisine (const TSolution current, TProblem instance, TAlgo &algo, int swapPosition, int destinationPosition);

//DESCRIPTION: Échange de deux villes sélectionnées aléatoirement. NB:uneSol ne doit pas être modifiée
void runRandomSwapLoop(TSolution &current, TSolution &next, TProblem instance, TAlgo &algo);
TSolution randomSwap (const TSolution current, TProblem instance, TAlgo &algo);

//DESCRIPTION: Échange de deux villes sélectionnées itérativement
void runIterativeSwapLoopFirstImprove(TSolution &current, TSolution &next, TProblem instance, TAlgo &algo);
void runIterativeSwapLoopBestImprove(TSolution &current, TSolution &next, TProblem instance, TAlgo &algo);
TSolution iterativeSwap (const TSolution current, TProblem instance, TAlgo &algo, const int indexSource, const int indexDestination);

//DESCRIPTION: Échange de deux villes sélectionnées progressivement (adjacence dans la solution courante)
void runProgressiveSwapLoop(TSolution &current, TSolution &next, TProblem instance, TAlgo &algo);
TSolution progressiveSwap (const TSolution current, TProblem instance, TAlgo &algo, const int indexSource);

//DESCRIPTION: Ensemble de méthodes utilitaires (affichage, échange d'éléments dans une liste, ...)
void swapElement(TSolution &seq, const int index1, const int index2);
void printEvaluation(const TSolution &current, const TAlgo &algo);
void printString(const std::string message);
TSolution findBestSolutionFromArray(const vector<TSolution> &kResults);

// ========================================================================================== //
//                                            MAIN                                            //
// ========================================================================================== //

int main(int NbParam, char *params[])
{
	TSolution initial;  //Solution active au cours des itérations
	TSolution next;		//Solution voisine retenue à une itération
	TSolution best;		//Meilleure solution depuis le début de l'algorithme
	TProblem instance;	//Définition de l'instance de problème
	TAlgo algo;			//Définition des paramètres de l'agorithme
	string filename;
			
	// Lecture des paramètres & du fichier de donnees
	filename.assign(params[1]);
	algo.NB_EVAL_MAX= atoi(params[2]);
	LectureProbleme(filename, instance, algo);
	
	// Création de la solution initiale 
	CreerSolutionAleatoire(initial, instance, algo);
	AfficherSolution(initial, algo, instance.Nb, "SolInitiale: ");

	// Resolution du problème en utilisant l'algorithme spécifié
	findBestAlternative(initial, next, best, instance, algo, SWAP_ITERATIVE_BEST_IMPROVE);

	// Affichage et sauvegarde des résultats
	AfficherResultats(best, instance, algo);
	AfficherResultatsFichier(best, instance, algo,"Resultats.txt");
	
	// Nettoyage de la mémoire et arrêt
	LibererMemoireFinPgm(initial, next, best, instance);
	system("PAUSE");
	return 0;
}

void findBestAlternative(TSolution &current, TSolution &next, TSolution &best, TProblem instance, TAlgo &algo, Type type)
{
	// Invoke algorithm for the inital run
	invokeAlgorithm(current, next, best, instance, algo, type);
	cout << "	[INITIAL RUN] Current best solution scored " << current.FctObj << endl;
	
	// Loop until it reaches maxiumum number of evaluations
	int rerunCount = 0;
	while(algo.CptEval < algo.NB_EVAL_MAX) {
		rerunCount++;
		
		// Duplicate current best
		TSolution rerunBest;
		CopierSolution(best, rerunBest, instance);
		
		// Invoke algorithm using another random solution
		CreerSolutionAleatoire(current, instance, algo);
		invokeAlgorithm(current, next, rerunBest, instance, algo, type);
		cout << "	[RERUN #" << rerunCount << "] Scored " << rerunBest.FctObj << " (reference: " << best.FctObj << ")";
		
		// Check if there is any improvement
		//	 - [Improvement]: override best result using rerun result
		//   - [No improvement]: simply discard rerun result
		if(rerunBest.FctObj < best.FctObj) {
			cout << endl << "	[IMPROVEMENT] Using rerun result as new current best" << endl;
			best = rerunBest;
			cout << "	[STATUS] " << algo.CptEval << " evaluations have been used out of " << algo.NB_EVAL_MAX << " (REMAINING: " << algo.NB_EVAL_MAX-algo.CptEval << ")" << endl;
		} else {
			cout << " > Discarding result" << endl;
		}
	}
}

void invokeAlgorithm(TSolution &current, TSolution &next, TSolution &best, TProblem instance, TAlgo &algo, Type type)
{
	switch(type) {
		
		case SWAP_RANDOM:
			printString("=========================== RUNNING RANDOM SWAP ===========================");
			runRandomSwapLoop(current, next, instance, algo);
			break;

		case SWAP_ITERATIVE_FIRST_IMPROVE:
			printString("=========================== RUNNING ITERATIVE SWAP (FIRST-IMPROVE) ===========================");
			// [A][B]CDE > [B][A]CDE > [C]B[A]DE > [D]BC[A]E > [E]BCD[A] > [B][A]CDE > A[C][B]DE > A[D]C[B]E > A[E]CD[B] > ...
			runIterativeSwapLoopFirstImprove(current, next, instance, algo);
			break;

		case SWAP_ITERATIVE_BEST_IMPROVE:
			printString("=========================== RUNNING ITERATIVE SWAP (BEST-IMPROVE) ===========================");
			// Same as SWAP_ITERATIVE but using a Best-Improve approach
			runIterativeSwapLoopBestImprove(current, next, instance, algo);
			break;

		case SWAP_PROGRESSIVE:
			printString("=========================== RUNNING PROGRESSIVE SWAP ===========================");
			// [A][B]CDE > [B][A]CDE > A[C][B]DE > AB[D][C]E > ABC[E][D] > [C]B[A]DE > A[D]C[B]E > AB[E]D[C] > [D]BC[A]E > A[E]CD[B]
			runProgressiveSwapLoop(current, next, instance, algo);
			break;

		default:
			break;
	}

	best = current;
}



// ========================================================================================== //
//                                      SOLUTION VOISINE                                      //
// ========================================================================================== //

//DESCRIPTION: Création d'une solution voisine à partir de la solution uneSol qui ne doit pas être modifiée.
//Dans cette fonction, on appel le TYPE DE VOISINAGE sélectionné + on détermine la STRATÉGIE D'ORIENTATION. 
//Ainsi, si la RÈGLE DE PIVOT nécessite l'étude de plusieurs voisins, la fonction TYPE DE VOISINAGE sera appelée plusieurs fois.
//Le TYPE DE PARCOURS dans le voisinage interviendra normalement dans la fonction TYPE DE VOISINAGE.
TSolution GetSolutionVoisine (const TSolution current, TProblem instance, TAlgo &algo)
{
	//Type de voisinage : à indiquer (Echange 2 villes aléatoires)
	//Parcours dans le voisinage : à indiquer	(Aleatoire)
	//Règle de pivot : à indiquer	(First-Impove)

	TSolution unVoisin;

	unVoisin = randomSwap(current, instance, algo);
	return (unVoisin);	
}

// ========================================================================================== //
//                                         RANDOM SWAP                                        //
// ========================================================================================== //

void runRandomSwapLoop(TSolution &current, TSolution &next, TProblem instance, TAlgo &algo)
{
	do { 
		next = randomSwap(current, instance, algo);
		if (next.FctObj < current.FctObj) {
			current = next;
			printEvaluation(current, algo);
		}
	} while (algo.CptEval < algo.NB_EVAL_MAX);
}

// Échange de deux villes sélectionnées aléatoirement
TSolution randomSwap (const TSolution current, TProblem instance, TAlgo &algo)
{
	TSolution copie;
	int posA, posB;
	CopierSolution(current, copie, instance);
	
	// Tirage aléatoire des 2 positions & échange
	posA = rand() % instance.Nb;
	do { posB = rand() % instance.Nb; } while (posA == posB);
	swapElement(copie, posA, posB);

	// Évaluation du nouveau voisin
	copie.FctObj = EvaluerSolution(copie, instance, algo);
	return(copie);
}

// ========================================================================================== //
//                               ITERATIVE SWAP (FIRST IMPROVE)                               //
// ========================================================================================== //

void runIterativeSwapLoopFirstImprove(TSolution &current, TSolution &next, TProblem instance, TAlgo &algo)
{
	int indexSource = 0;
	int indexDestination = 1;
	do {
		
		// Swap source and destination elements 
		//	 - Destination index changes every loop run
		//   - Source index only changes if destination index reached the end
		next = iterativeSwap(current, instance, algo, indexSource, indexDestination);
		
		// Check if there is any improvement
		//	 - [Improvement]: update current result and reset source index
		//   - [No improvement]: try all permutations using next element
		if (next.FctObj < current.FctObj) {
			current = next;
			indexSource = 0;
			indexDestination = 1;
		} else {
			if(++indexDestination == current.Seq.size()) {
				indexSource++;
				indexDestination = 0;
			}
		}
	} while (algo.CptEval < algo.NB_EVAL_MAX && (indexSource != current.Seq.size()-1 && indexDestination != current.Seq.size()));
}

TSolution iterativeSwap (const TSolution current, TProblem instance, TAlgo &algo, const int indexSource, const int indexDestination)
{
	TSolution copie;
	CopierSolution(current, copie, instance);
	swapElement(copie, indexSource, indexDestination);

	// Évaluation du nouveau voisin
	copie.FctObj = EvaluerSolution(copie, instance, algo);
	return(copie);
}

// ========================================================================================== //
//                                ITERATIVE SWAP (BEST IMPROVE)                               //
// ========================================================================================== //

void runIterativeSwapLoopBestImprove(TSolution &current, TSolution &next, TProblem instance, TAlgo &algo)
{
	int indexSource = 0;
	vector<TSolution> kResults;
	kResults.reserve(current.Seq.size() - 1);
	
	do {
		// For a given element, try all permutations and store them in an array 
		for(unsigned int i=0; i<current.Seq.size(); i++) {
			if(indexSource != i) {
				kResults.push_back(iterativeSwap(current, instance, algo, indexSource, i));
			}
		}

		// Find the best solution from the resulting array
		next = findBestSolutionFromArray(kResults);

		// Check if there is any improvement
		//	 - [Improvement]: update current result and reset source index
		//   - [No improvement]: try all permutations using next element
		if(next.FctObj < current.FctObj) {
			current = next;
			indexSource = 0;
		} else {
			indexSource++;
		}

		// Reset the array (clear solutions and reserve space)
		kResults.clear();
		kResults.reserve(current.Seq.size() - 1);
	} while (algo.CptEval < algo.NB_EVAL_MAX && indexSource != current.Seq.size());
}

// ========================================================================================== //
//                                      PROGRESSIVE SWAP                                      //
// ========================================================================================== //

void runProgressiveSwapLoop(TSolution &current, TSolution &next, TProblem instance, TAlgo &algo)
{
	int indexSource = 0;
	int indexDistance = 1;

	do {
		
		// Swap source and destination elements 
		//	 - Source & Destination indexes change every loop run
		next = iterativeSwap(current, instance, algo, indexSource, indexSource + indexDistance);
		
		// Check if there is any improvement
		//	 - [Improvement]: update current result and reset source and destination indexes
		//   - [No improvement]: move both source and destination indexes
		if (next.FctObj < current.FctObj) {
			current = next;
			indexSource = 0;
			indexDistance = 1;
		} else {
			if(++indexSource + indexDistance == current.Seq.size()) {
				indexSource = 0;
				indexDistance++;
			}
		}
	} while (algo.CptEval < algo.NB_EVAL_MAX && (indexSource != current.Seq.size()-1 && indexSource + indexDistance != current.Seq.size()));
}

TSolution progressiveSwap (const TSolution current, TProblem instance, TAlgo &algo, const int indexSource)
{
	TSolution copie;
	CopierSolution(current, copie, instance);
	swapElement(copie, indexSource, indexSource + 1);

	// Évaluation du nouveau voisin
	copie.FctObj = EvaluerSolution(copie, instance, algo);
	return(copie);
}

// ========================================================================================== //
//                                            UTILS                                           //
// ========================================================================================== //

void swapElement(TSolution &seq, const int index1, const int index2) 
{
	int tmpValue = seq.Seq[index1];
	seq.Seq[index1] = seq.Seq[index2];
	seq.Seq[index2] = tmpValue;
}

void printEvaluation(const TSolution &current, const TAlgo &algo)
{
	cout << "CptEval: " << algo.CptEval << " Fct Obj Nouvelle Courante: " << current.FctObj << endl;
}

void printString(const std::string message)
{
	cout << endl << message << endl << endl;
}

TSolution findBestSolutionFromArray(const vector<TSolution> &kResults)
{
	TSolution best = kResults[0];
	for(unsigned int i=1; i<kResults.size(); i++) {
		if(best.FctObj > kResults[i].FctObj) {
			best = kResults[i];
		}
	}
	return best;
}